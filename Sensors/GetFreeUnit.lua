local sensorInfo = {
	name = "GetFreeUnit",
	desc = "Returns a free unit",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return free unit from res table
return function(table, sortedList)
	freeUnit = nil
	--for unit, value in pairs(table) do
	--	if value == "free" then
	--		freeUnit = unit
	--		break
	--	end
	--end
	for _, unit in pairs(sortedList) do
		if table[unit] == "free" then
			freeUnit = unit
			break
		end
	end

	if freeUnit ~= nil then
		table[freeUnit] = "occupied"
	end

	return freeUnit
end