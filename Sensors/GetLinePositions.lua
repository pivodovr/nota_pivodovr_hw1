local sensorInfo = {
	name = "GetLinePositions",
	desc = "Return positions of units forming a line.",
	author = "Renata Pivodova",
	date = "2022-05",
}
local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the line formation
-- @argument numberOfUnits [int] number of units in formation
-- @argument coordinate [int] line's stable coordinate 
return function(numberOfUnits, coordinate)
    local step = Game.mapSizeX / numberOfUnits
    -- positions in line formation
	local allPositions = {}
    for index=1, numberOfUnits do
        allPositions[index] = Vec3((index-1) * step, 0, coordinate)
    end
	
	return allPositions
end


