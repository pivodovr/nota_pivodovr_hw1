local sensorInfo = {
	name = "SortByDistance",
	desc = "Returns list of units sorted by distance.",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- original implementation of quicksort here: https://github.com/mirven/lua_snippets/blob/master/lua/quicksort.lua

function partition(array, left, right, pivotIndex)
    local x, y, z = Spring.GetUnitPosition(array[pivotIndex])
	local pivotValue = z
	array[pivotIndex], array[right] = array[right], array[pivotIndex]
	
	local storeIndex = left
	
	for i =  left, right-1 do
        local x1, y2, z2 = Spring.GetUnitPosition(array[i])
    	if z2 > pivotValue then
	        array[i], array[storeIndex] = array[storeIndex], array[i]
	        storeIndex = storeIndex + 1
		end
		array[storeIndex], array[right] = array[right], array[storeIndex]
	end
	
   return storeIndex
end

function quicksort(array, left, right)
	if right > left then
	    local pivotNewIndex = partition(array, left, right, left)
	    quicksort(array, left, pivotNewIndex - 1)
	    quicksort(array, pivotNewIndex + 1, right)
	end
end

return function(array)
    quicksort(array, 1, #array)
end