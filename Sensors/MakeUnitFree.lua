local sensorInfo = {
	name = "MakeUnitFree",
	desc = "Make given unit free and update reservation table",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return updated table with free given unit
return function(table, unit)
    -- make unit free
	table[unit] = "free"

    -- update table by removing dead units
	for u, _ in pairs(table) do
		if not Spring.ValidUnitID(u) then
			table[u] = nil
		end
	end
    return table
end