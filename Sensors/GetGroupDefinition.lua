local sensorInfo = {
	name = "GetGroupDefinition",
	desc = "Returns a group definition for formation.moveCustomGroup.",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns a group definition of given list of units
return function(units)
	local groupDefinition = {}

	for index, value in ipairs(units) do
		groupDefinition[value] = index
	end

	return groupDefinition
end