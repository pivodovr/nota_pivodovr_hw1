local sensorInfo = {
	name = "InitResTable",
	desc = "Initialize reservation table",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return new reservation table for given list
return function(list)
	newList = {}

	for i = 1, #list do
		newList[list[i]] = "free"
	end

	return newList
end