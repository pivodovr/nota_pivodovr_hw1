local sensorInfo = {
	name = "DefinitionLine",
	desc = "Return definition of the line formation",
	author = "Renata Pivodova",
	date = "2022-05",
}
local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return stuctured description of the line formation
-- @argument numberOfUnits [int] number of units in formation
return function(numberOfUnits)
    local step = Game.mapSizeX / numberOfUnits
    -- positions in line formation
	local allPositions = {}
    for index=1, numberOfUnits+1 do
        allPositions[index] = Vec3(0, 0, (index-1) * step)
    end
	
	-- do not rewrite the originial table otherwise it is not robust on "reset"
	local finalDefinition = {
		name = "line",
		positions = allPositions,
		generated = false,
		defaults = {
			spacing = Vec3(10, 1 ,10), -- ????
			hillyCoeficient = 30,
			constrained = true,
			variant = false,
			rotable = false,
		},	
	}
	
	return finalDefinition
end


