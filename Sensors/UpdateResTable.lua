local sensorInfo = {
	name = "UpdateResTable",
	desc = "Update reservation table",
	author = "Renata Pivodova",
	date = "2022-06",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return updated table with free given unit
return function(table, safeArea)
    -- update table by making free occupied units outside the safe area
	unitsInArea = Spring.GetUnitsInSphere(safeArea.center.x, safeArea.center.y, safeArea.center.z, safeArea.radius)
	for unit, state in pairs(table) do
		if Spring.GetUnitTransporter(unit) == nil and state == "occupied" then
			notSaved = true
			for _, unitInArea in ipairs(unitsInArea) do 
				if unit == unitInArea then
					notSaved = false
					break
				end
			end
			if notSaved == true then
				table[unit] = "free"
			end
		end
	end
end