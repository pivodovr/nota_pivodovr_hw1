local sensorInfo = {
	name = "Hills",
	desc = "Return all known hills.",
	author = "Renata Pivodova",
    date = "2022-05"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function areCloseObjects(objAPos, objBPos)
    local distance = math.sqrt((objAPos.x - objBPos.x)^2 + (objAPos.z - objBPos.z)^2) 
    if distance < 141 then
        return true
    end
    return false
end

-- @description Check if candidate hill has been already discovered (and recorded in table).
function notDiscovered(candidatePos, hills)
    if hills == {} then 
        return true
    end
    for i, hillPos in ipairs(hills) do
        if areCloseObjects(candidatePos, hillPos) then
            return false
        end
    end
    return true
end 

-- @description Return positions of all hills divided into two categories: safe hill and hill with enemy.
return function(hillHeight, enemyPosition)
    local withEnemy = {}
    local safe = {}
    local allHills = {}
    local step = 100
    for x = 0, Game.mapSizeX, step do
        for z = 0, Game.mapSizeZ, step do
            local y = Spring.GetGroundHeight(x,z)
            if y == hillHeight then
                local candidateHill = Vec3(x, y, z)
                if notDiscovered(candidateHill, allHills) then
                    table.insert(allHills, candidateHill)
                    if areCloseObjects(candidateHill, enemyPosition) then
                        table.insert(withEnemy, candidateHill)
                    else 
                        table.insert(safe, candidateHill)
                    end
                end
            end
        end
    end
    return {
        withEnemy = withEnemy,
        safe = safe
    }
end