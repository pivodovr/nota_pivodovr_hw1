local sensorInfo = {
	name = "GetUnitPosition",
	desc = "Get position of given unit.",
	author = "Renata Pivodova",
	date = "2022-05"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return position of the unit
return function(unitID)
	x, y, z = Spring.GetUnitPosition(unitID)
	return {x = x,y = y,z = z}
end