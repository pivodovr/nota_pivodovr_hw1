local sensorInfo = {
	name = "FilterCloseU2R",
	desc = "Returns an updated list of close units to safe rescue.",
	author = "Renata Pivodova",
	date = "2022-05",
	license = "none",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(U2RList)
    -- filter close units to rescue
    local filtered = {}
    index = 1 
    for _, unit in pairs(U2RList) do
        local x, y, z = Spring.GetUnitPosition(unit)
        if z < 6000 and x > 300 then
            filtered[index] = unit
            index = index + 1
        end
    end

    return filtered
end