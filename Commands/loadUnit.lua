function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load one unit to a transporter.",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
    -- FAILURE CONDITIONS
    -- non-valid unit IDs
    if not Spring.ValidUnitID(parameter.unit) or not Spring.ValidUnitID(parameter.transporter) then
        return FAILURE
    end

    -- dead units
    if Spring.GetUnitIsDead(parameter.unit) or Spring.GetUnitIsDead(parameter.transporter) then
        return FAILURE
    end
    
    -- unit is carried by someone else
    local trueTransporter = Spring.GetUnitTransporter(parameter.unit)

    if trueTransporter ~= nil and trueTransporter ~= parameter.transporter then
        return FAILURE
    end

    -- transporter is not transporter type
    if not UnitDefs[Spring.GetUnitDefID(parameter.transporter)].isTransport then
        return FAILURE
    end

    -- SUCCESS CONDITIONS
    if trueTransporter == parameter.transporter then
        return SUCCESS
    end

    -- INIT
    if not self.initialized then
        Spring.GiveOrderToUnit(parameter.transporter,  CMD.LOAD_UNITS,{parameter.unit},{"shift"})
        self.initialized = true
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end
    
