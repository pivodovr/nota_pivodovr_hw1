function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Explore area with given units from current positions to given ending positions.",
		parameterDefs = {
			{ 
				name = "explorers", -- table of explorers IDs
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "endingPositions", -- table of ending positions
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
    local explorers = parameter.explorers
    local positions = parameter.endingPositions
    -- INIT
    if not self.initialized then 
        for index, explorer in pairs(explorers) do
            Spring.GiveOrderToUnit(explorer, CMD.MOVE, {positions[index].x, positions[index].y, positions[index].z}, {"shift"})
        end
        self.initialized = true
        return RUNNING
    else 
        -- SUCCESS CONDS
        local successbool = true
        for index, explorer in pairs(explorers) do
            if Spring.GetUnitCommands(explorer) ~= nil then -- if explorer is not dead
                if Spring.GetUnitCommands(explorer)[1] ~= nil then -- if explorer is executing an order
                    successbool = false
                end
            end
        end

        if successbool == true then
            return SUCCESS
        else 
            return RUNNING
        end
    end
end

function Reset(self)
    self.initialized = false
end