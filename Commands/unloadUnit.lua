function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Unload a transporter in given area.",
        parameterDefs = {
            { 
                name = "transporter",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
				name = "area",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
        }
    }
end

function Run(self, units, parameter)
    -- FAILURE CONDITIONS
    -- non-valid unit ID
    if not Spring.ValidUnitID(parameter.transporter) then
        return FAILURE
    end
    
    -- transporter is dead
    if Spring.GetUnitIsDead(parameter.transporter) then
        return FAILURE
    end

    -- transporter is not transporter type
    if not UnitDefs[Spring.GetUnitDefID(parameter.transporter)].isTransport then
        return FAILURE
    end

    -- SUCCES CONDITIONS
    if #Spring.GetUnitIsTransporting(parameter.transporter) == 0 then 
        return SUCCESS
    end

    -- INIT
    if not self.initialized then
        Spring.GiveOrderToUnit(parameter.transporter, CMD.UNLOAD_UNITS, {parameter.area.center.x, parameter.area.center.y, parameter.area.center.z, parameter.area.radius}, {"shift"})
		self.initialized = true
    end

    return RUNNING
end 

function Reset(self)
    self.initialized = false
end