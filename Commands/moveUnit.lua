function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move given unit to defined position.",
        parameterDefs = {
            { 
                name = "unit",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

function Run(self, units, parameter)
    local position = parameter.position

    -- FAILURE CONDITIONS
    -- non-valid unit IDs
    if not Spring.ValidUnitID(parameter.unit) then
        Spring.Echo("Non-valid ID.")
        return FAILURE
    end

    -- dead unit
    if Spring.GetUnitIsDead(parameter.unit) then
        Spring.Echo("Dead unit.")
        return FAILURE
    end

    -- unit is immobile 
    if UnitDefs[Spring.GetUnitDefID(parameter.unit)].isImmobile then
        Spring.Echo("Immobile unit cannot move.")
        return FAILURE
    end

    -- defined position is invalid
    if position.x == nil or position.z == nil then
        Spring.Echo("Invalid position.")
        return FAILURE
    end

    -- SUCCESS CONDITIONS
    local x, y, z = Spring.GetUnitPosition(parameter.unit)
    local unitPosition = Vec3(x,y,z)
    if unitPosition:Distance(position) < 100 then
        return SUCCESS
    end
    
    -- INIT
    if 
        not self.initialized or
        unitPosition:Distance(self.lastPosition) < 1
    then
        Spring.GiveOrderToUnit(parameter.unit, CMD.MOVE, position:AsSpringVector(), {"shift"})
        self.initialized = true
    end
    
    self.lastPosition = unitPosition

    return RUNNING
end

function Reset(self)
    self.initialized = false
end