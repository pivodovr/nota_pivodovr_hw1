nota_pivodovr 3.2
====
2022

Implementation of:

* sandsail homework with following soldiers,
* ctp2 homework with capturing hill with enemy,
* singlresc which is the building block for ttdr.
* ttdr homework with basic rescue.


* [dependencies](./dependencies.json)

Trees
----

* sandsail
* ctp2
* singlresc
* exploreMap
* ttdr

Sensors
----

* GetHills
* DefinitionLine
* FilterCloseU2R
* GetAvailableUnit
* GetFormation
* GetFreeUnit
* GetGroupDefinition
* GetLinePositions
* GetUnitPosition
* InitResTable
* MakeUnitFree
* SortByDistance
* UpdateResTable

Commands
----

* loadUnit
* unloadUnit
* explore



